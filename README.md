# README #

pCloudy is the next-gen mobile app testing platform with more than 5000 device browser combinations. Mobile app testing is critical for an app to get popular and provides a good user experience. pCloudy solves the issue of testing an app on multiple devices with different specifications and saves you from spending a huge amount of money on building a device lab for the same purpose.      
Manual and Automation testing in pCloudy
You can test your app using popular Android and iOS app testing tools on pcloudy devices. Most popular Android and iOS automation testing tools like Appium, Espresso, Calabash etc are integrated with pCloudy. Scale your Automation like never before by running parallel scripts. Utilize the power of testing on real devices with CI. You can use Jenkins plug-in, or our APIs to integrate with other tools. 
https://www.pcloudy.com